fn calculate_part_one(input: Vec<i32>) -> Option<i32> {
    Some(calculate_increments(input))
}

fn calculate_increments(input: Vec<i32>) -> i32 {
    input.windows(2)
        .map(|v| (v.first(), v.last()))
        .map(|(lhs, rhs)| (lhs.unwrap_or(&0), rhs.unwrap_or(&0)))
        .fold(0, |acc, (lhs, rhs)| {
            if lhs < rhs {
                acc + 1
            } else {
                acc
            }
        })
}

fn calculate_part_two(input: Vec<i32>) -> Option<i32> {
    let values: Vec<i32> = input.windows(3)
        .map(|v| v.iter().sum())
        .collect();

    Some(calculate_increments(values))
}

#[cfg(test)]
mod tests {
    use adventofcode::{map_parse, read_lines_from_file};

    use super::*;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2021/day_1_1_example.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(7);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2021/day_1_1.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(1215);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example() {
        let path = "../../input/2021/day_1_2_example.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(5);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2021/day_1_2.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(1150);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
