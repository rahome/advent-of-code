use std::collections::HashSet;
use std::ops::Add;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn from(direction: char) -> Self {
        if direction == '^' {
            Point { x: 0, y: 1 }
        } else if direction == '>' {
            Point { x: 1, y: 0 }
        } else if direction == 'v' {
            Point { x: 0, y: -1 }
        } else if direction == '<' {
            Point { x: -1, y: 0 }
        } else {
            Point::empty()
        }
    }

    fn empty() -> Self {
        Point { x: 0, y: 0 }
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

fn calculate_part_one(input: String) -> Option<isize> {
    let mut visited: HashSet<Point> = HashSet::new();
    let mut moves = vec![Point::empty()];
    moves.extend(input.chars().map(Point::from).collect::<Vec<Point>>());
    moves.into_iter().fold(Point::empty(), |left, right| {
        let v = left + right;
        visited.insert(v);
        v
    });

    Some(visited.len() as isize)
}

fn calculate_part_two(input: String) -> Option<isize> {
    let mut santa_position = Point::empty();
    let mut robot_position = Point::empty();
    let mut visited: HashSet<Point> = HashSet::from([santa_position, robot_position]);
    let mut moves = vec![Point::empty()];
    moves.extend(input.chars().map(Point::from).collect::<Vec<Point>>());

    let mut is_santa_turn = true;
    for direction in moves {
        if is_santa_turn {
            santa_position = santa_position + direction;
            visited.insert(santa_position);
        } else {
            robot_position = robot_position + direction;
            visited.insert(robot_position);
        }

        is_santa_turn = !is_santa_turn;
    }

    Some(visited.len() as isize)
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn part_one_with_example_one() {
        let path = "../../input/2015/day_3_1_example_1.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(2);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_two() {
        let path = "../../input/2015/day_3_1_example_2.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(4);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_three() {
        let path = "../../input/2015/day_3_1_example_3.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(2);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2015/day_3_1.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(2565);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_one() {
        let path = "../../input/2015/day_3_2_example_1.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(3);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_two() {
        let path = "../../input/2015/day_3_2_example_2.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(3);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_three() {
        let path = "../../input/2015/day_3_2_example_3.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(11);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2015/day_3_2.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(2639);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
