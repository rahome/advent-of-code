use std::collections::HashSet;

fn calculate_part_one(input: Vec<String>) -> Option<usize> {
    let v = input
        .into_iter()
        .filter(|x| {
            contains_required_number_of_vowels(&x)
                && contains_consecutive_characters(&x)
                && !contains_disallowed_character_groups(&x)
        })
        .collect::<Vec<String>>();

    Some(v.len())
}

fn contains_required_number_of_vowels(line: &str) -> bool {
    let vowels: Vec<char> = vec!['a', 'e', 'i', 'o', 'u'];
    let found_vowels = line
        .chars()
        .filter(|c| vowels.contains(c))
        .collect::<Vec<char>>();

    found_vowels.len() >= 3
}

fn contains_consecutive_characters(line: &str) -> bool {
    for char in HashSet::<char>::from_iter(line.chars()) {
        if line.contains(&format!("{}{}", char, char)) {
            return true;
        }
    }
    false
}

fn contains_disallowed_character_groups(line: &str) -> bool {
    let disallowed_character_groups: Vec<&str> = vec!["ab", "cd", "pq", "xy"];
    for disallowed_character_group in disallowed_character_groups {
        if line.contains(disallowed_character_group) {
            return true;
        }
    }
    false
}

fn calculate_part_two(input: Vec<String>) -> Option<usize> {
    let v = input
        .into_iter()
        .filter(|x| {
            contains_duplicate_character_groups(&x) && contains_single_character_separator(&x)
        })
        .collect::<Vec<String>>();

    Some(v.len())
}

fn contains_duplicate_character_groups(line: &str) -> bool {
    for (a, b) in line.chars().zip(line.chars().skip(1)) {
        if line.matches(&format!("{}{}", a, b)).count() >= 2 {
            return true;
        }
    }
    false
}

fn contains_single_character_separator(line: &str) -> bool {
    for (a, b) in line.chars().zip(line.chars().skip(2)) {
        if a == b {
            return true;
        }
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;
    use adventofcode::read_lines_from_file;

    #[test]
    fn part_one_with_example_one() {
        let path = "../../input/2015/day_5_1_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(1);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_two() {
        let path = "../../input/2015/day_5_1_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(1);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_three() {
        let path = "../../input/2015/day_5_1_example_3.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(0);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_four() {
        let path = "../../input/2015/day_5_1_example_4.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(0);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_five() {
        let path = "../../input/2015/day_5_1_example_5.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(0);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2015/day_5_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(238);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_one() {
        let path = "../../input/2015/day_5_2_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(1);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_two() {
        let path = "../../input/2015/day_5_2_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(1);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_three() {
        let path = "../../input/2015/day_5_2_example_3.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(0);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_four() {
        let path = "../../input/2015/day_5_2_example_4.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(0);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2015/day_5_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(69);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
