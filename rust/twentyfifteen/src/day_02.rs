use adventofcode::map_parse;

const INDEX_LENGTH: usize = 0;
const INDEX_HEIGHT: usize = 1;
const INDEX_WIDTH: usize = 2;

struct Cube {
    length: isize,
    height: isize,
    width: isize,
}

impl Cube {
    fn from(values: Vec<isize>) -> Self {
        Cube {
            length: values.get(INDEX_LENGTH).unwrap_or(&1).clone(),
            height: values.get(INDEX_HEIGHT).unwrap_or(&1).clone(),
            width: values.get(INDEX_WIDTH).unwrap_or(&1).clone(),
        }
    }

    fn dimensions(&self) -> Vec<isize> {
        let mut dimensions = vec![self.length, self.width, self.height];
        dimensions.sort();
        dimensions
    }

    fn sides(&self) -> Vec<isize> {
        vec![
            self.length * self.width,
            self.width * self.height,
            self.height * self.length,
        ]
    }

    fn size(&self) -> isize {
        self.sides().iter().map(|v| 2 * v).sum::<isize>()
    }

    fn smallest_side(&self) -> isize {
        self.sides().iter().min().unwrap_or(&0).clone()
    }
}

fn calculate_part_one(input: Vec<String>) -> Option<isize> {
    let result = input
        .into_iter()
        .map(|x| {
            let v = parse_dimensions(&x);
            let cube = Cube::from(v);
            calculate_necessary_feet_of_wrapping_paper(&cube)
        })
        .sum();

    Some(result)
}

fn parse_dimensions(x: &String) -> Vec<isize> {
    let values = x.split("x").map(|v| v.to_string()).collect();
    map_parse::<isize>(&values)
}

fn calculate_necessary_feet_of_wrapping_paper(cube: &Cube) -> isize {
    cube.size() + cube.smallest_side()
}

fn calculate_part_two(input: Vec<String>) -> Option<isize> {
    let result = input
        .into_iter()
        .map(|x| {
            let v = parse_dimensions(&x);
            let cube = Cube::from(v);
            let feet_for_ribbon = calculate_necessary_feet_of_ribbon(&cube);
            let feet_for_bow = calculate_necessary_feet_of_ribbon_for_bow(&cube);
            feet_for_ribbon + feet_for_bow
        })
        .sum();

    Some(result)
}

fn calculate_necessary_feet_of_ribbon(cube: &Cube) -> isize {
    cube.dimensions()
        .into_iter()
        .reduce(|a, b| a * b)
        .unwrap_or(0)
}

fn calculate_necessary_feet_of_ribbon_for_bow(cube: &Cube) -> isize {
    cube.dimensions().into_iter().take(2).sum::<isize>() * 2
}

#[cfg(test)]
mod tests {
    use adventofcode::read_lines_from_file;

    use super::*;

    #[test]
    fn part_one_with_example_one() {
        let path = "../../input/2015/day_2_1_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(58);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_two() {
        let path = "../../input/2015/day_2_1_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(43);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2015/day_2_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(1606483);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_one() {
        let path = "../../input/2015/day_2_2_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(34);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_two() {
        let path = "../../input/2015/day_2_2_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(14);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2015/day_2_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(3842356);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
