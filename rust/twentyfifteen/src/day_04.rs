use md5::{Digest, Md5};

fn calculate_part_one(input: String) -> Option<isize> {
    let prefix = "00000";
    calculate_iterations_until_prefix_is_used(input, prefix)
}

fn calculate_part_two(input: String) -> Option<isize> {
    let prefix = "000000";
    calculate_iterations_until_prefix_is_used(input, prefix)
}

fn calculate_iterations_until_prefix_is_used(input: String, prefix: &str) -> Option<isize> {
    let secret_key = input.trim_end();
    let mut iterations: isize = 0;
    loop {
        let value = format!("{}{}", secret_key, iterations);
        let mut hasher = Md5::new();
        hasher.update(value.as_bytes());
        let hash = hasher.finalize();
        let value = base16ct::lower::encode_string(&hash);
        if value.starts_with(prefix) {
            return Some(iterations);
        }
        iterations += 1;
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn part_one_with_example_one() {
        let path = "../../input/2015/day_4_1_example_1.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(609043);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_two() {
        let path = "../../input/2015/day_4_1_example_2.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(1048970);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2015/day_4_1.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(117946);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2015/day_4_2.txt";
        let input = fs::read_to_string(path);
        let expected: Option<isize> = Some(3938038);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
