fn calculate_floor_movement(movements: &str) -> Vec<isize> {
    movements.chars()
        .map(|k| if k == '(' {
            1
        } else {
            -1
        })
        .collect()
}

fn calculate_part_one(input: Vec<String>) -> Option<isize> {
    input.first()
        .map(|x| calculate_floor_movement(x))
        .map(|x| x.into_iter().sum())
}

fn calculate_part_two(input: Vec<String>) -> Option<isize> {
    let movements: Vec<isize> = input.iter()
        .flat_map(|x| calculate_floor_movement(x))
        .collect();

    let mut current_floor: isize = 0;
    for (index, up_or_down) in movements.into_iter().enumerate() {
        current_floor = current_floor + up_or_down;
        if current_floor == -1 {
            return Some(index as isize + 1);
        }
    }
    Some(current_floor)
}

#[cfg(test)]
mod tests {
    use adventofcode::read_lines_from_file;

    use super::*;

    #[test]
    fn part_one_with_example_one() {
        let path = "../../input/2015/day_1_1_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(0);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_two() {
        let path = "../../input/2015/day_1_1_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(0);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_three() {
        let path = "../../input/2015/day_1_1_example_3.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(3);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_four() {
        let path = "../../input/2015/day_1_1_example_4.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(3);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_five() {
        let path = "../../input/2015/day_1_1_example_5.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(3);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_six() {
        let path = "../../input/2015/day_1_1_example_6.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(-1);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_seven() {
        let path = "../../input/2015/day_1_1_example_7.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(-1);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_eight() {
        let path = "../../input/2015/day_1_1_example_8.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(-3);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_example_nine() {
        let path = "../../input/2015/day_1_1_example_9.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(-3);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2015/day_1_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(74);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_one() {
        let path = "../../input/2015/day_1_2_example_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(1);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example_two() {
        let path = "../../input/2015/day_1_2_example_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(5);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2015/day_1_2.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(1795);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
