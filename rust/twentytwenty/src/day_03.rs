use std::ops::Add;

use adventofcode::character_at_index;

const CHARACTER_FOR_TREE: &str = "#";

#[derive(Debug, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

fn calculate_part_one(input: Vec<String>) -> u32 {
    let slopes: Vec<Point> = Vec::from([
        Point { x: 3, y: 1 }
    ]);

    calculate_answer(&input, slopes)
}

fn calculate_part_two(input: Vec<String>) -> u32 {
    let walks: Vec<Point> = Vec::from([
        Point { x: 1, y: 1 },
        Point { x: 3, y: 1 },
        Point { x: 5, y: 1 },
        Point { x: 7, y: 1 },
        Point { x: 1, y: 2 },
    ]);

    calculate_answer(&input, walks)
}

fn calculate_answer(input: &Vec<String>, walks: Vec<Point>) -> u32 {
    let mut result: u32 = 1;
    for walk in walks {
        result *= calculate_number_of_trees(input, walk)
    }
    result
}

fn calculate_number_of_trees(input: &Vec<String>, walk: Point) -> u32 {
    if input.len() == 0 {
        return 1;
    }

    let first = input.first()
        .expect("Expected first entry from Vec");
    let max_length = first.len() as i32;

    let mut trees = 0;
    let mut position = Point { x: 0, y: 0 };
    for row in input {
        let character = character_at_index(position.x % max_length, &row).unwrap();
        if character.eq(&CHARACTER_FOR_TREE.to_string()) {
            trees += 1;
        }

        position = position + walk;
    }
    trees
}

#[cfg(test)]
mod test {
    use adventofcode::read_lines_from_file;

    use super::*;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2020/day_3_1_example.txt";
        let input = read_lines_from_file(path);
        let expected: u32 = 7;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2020/day_3_1.txt";
        let input = read_lines_from_file(path);
        let expected: u32 = 234;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example() {
        let path = "../../input/2020/day_3_2_example.txt";
        let input = read_lines_from_file(path);
        let expected: u32 = 336;

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
