const EXPECTED_SUM: i32 = 2020;

fn calculate_part_one(input: Vec<i32>) -> Option<i32> {
    for lhs in &input {
        for rhs in &input {
            let sum = lhs + rhs;
            if sum == EXPECTED_SUM {
                return Some(lhs * rhs);
            }
        }
    }
    None
}

fn calculate_part_two(input: Vec<i32>) -> Option<i32> {
    for first in &input {
        for second in &input {
            for third in &input {
                let sum = first + second + third;
                if sum == EXPECTED_SUM {
                    return Some(first * second * third);
                }
            }
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use adventofcode::{map_parse, read_lines_from_file};

    use super::*;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2020/day_1_1_example.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(514579);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2020/day_1_1.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(326211);

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example() {
        let path = "../../input/2020/day_1_2_example.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(241861950);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2020/day_1_2.txt";
        let input = read_lines_from_file(path).map(|x| map_parse::<i32>(&x));
        let expected: Option<i32> = Some(131347190);

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
