use std::collections::HashSet;

fn calculate_part_one(input: Vec<String>) -> usize {
    input.join::<&str>("\n")
        .split_terminator("\n\n")
        .map(|v| { v.replace("\n", "") })
        .map(|v| count_unique_characters(v))
        .fold(0, |acc, v| acc + v)
}

fn count_unique_characters(v: String) -> usize {
    v.chars().collect::<HashSet<char>>().len()
}

#[cfg(test)]
mod tests {
    use adventofcode::read_lines_from_file;

    use super::*;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2020/day_6_1_example.txt";
        let input = read_lines_from_file(path);
        let expected: usize = 11;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2020/day_6_1.txt";
        let input = read_lines_from_file(path);
        let expected: usize = 6763;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }
}
