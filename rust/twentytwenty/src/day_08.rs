use crate::day_08::Operation::{ACC, JMP, NOP};
use std::collections::HashSet;

#[derive(Copy, Clone)]
enum Operation {
    NOP,
    ACC,
    JMP,
}

fn parse_operation(segments: &Vec<&str>) -> Operation {
    let operation = segments.first()
        .unwrap_or(&"nop")
        .to_owned()
        .to_string();

    if operation.eq("acc") {
        ACC
    } else if operation.eq("jmp") {
        JMP
    } else {
        NOP
    }
}

#[derive(Copy, Clone)]
struct Instruction {
    operation: Operation,
    argument: i32,
}

fn calculate_part_one(input: Vec<String>) -> i32 {
    filter_unique_instructions(&parse_instructions(input))
        .iter()
        .fold(0, calculate_accumulator)
}

fn calculate_part_two(input: Vec<String>) -> i32 {
    calculate_correct_instructions_set(parse_instructions(input))
        .iter()
        .fold(0, calculate_accumulator)
}

fn calculate_accumulator(accumulator: i32, instruction: &Instruction) -> i32 {
    match instruction.operation {
        ACC => accumulator + instruction.argument,
        _ => accumulator,
    }
}

fn parse_instructions(input: Vec<String>) -> Vec<Instruction> {
    input.iter()
        .map(|v| parse_instruction(Some(v)))
        .collect()
}

fn parse_instruction(value: Option<&String>) -> Instruction {
    let instruction = value.unwrap_or(&String::from("nop +0")).to_owned();
    let segments = instruction.split(' ').collect::<Vec<_>>();

    Instruction {
        operation: parse_operation(&segments),
        argument: parse_argument(segments),
    }
}

fn parse_argument(segments: Vec<&str>) -> i32 {
    segments.last().unwrap_or(&"+0")
        .to_owned()
        .parse::<i32>()
        .unwrap_or(0)
}

fn filter_unique_instructions(instructions: &Vec<Instruction>) -> Vec<Instruction> {
    let mut unique_instructions: Vec<Instruction> = Vec::new();

    let mut executed_indexes: HashSet<usize> = HashSet::new();
    let mut current_index: usize = 0;
    loop {
        if executed_indexes.contains(&current_index) {
            break;
        }
        executed_indexes.insert(current_index);

        let instruction = read_instruction_at_index(instructions, current_index);
        current_index = calculate_next_index(current_index, instruction);
        unique_instructions.push(instruction.to_owned());
    }

    unique_instructions
}

fn calculate_correct_instructions_set(instructions: Vec<Instruction>) -> Vec<Instruction> {
    let mut unique_instructions: Vec<Instruction> = Vec::new();

    let mut executed_indexes: Vec<usize> = Vec::new();
    let mut current_index: usize = 0;
    let number_of_instructions = instructions.len();
    loop {
        if current_index >= number_of_instructions {
            break;
        }

        let mut instruction = read_instruction_at_index(&instructions, current_index);
        if current_index == (number_of_instructions - 2) {
            instruction = match instruction.operation {
                JMP => Instruction {
                    operation: NOP,
                    argument: instruction.argument,
                },
                NOP => Instruction {
                    operation: JMP,
                    argument: instruction.argument,
                },
                _ => instruction
            }
        }
        executed_indexes.push(current_index);

        current_index = calculate_next_index(current_index, instruction);
        unique_instructions.push(instruction.to_owned());
    }

    unique_instructions
}

fn read_instruction_at_index(instructions: &Vec<Instruction>, current_index: usize) -> Instruction {
    instructions.get(current_index)
        .unwrap_or(&Instruction { operation: NOP, argument: 0 })
        .to_owned()
}

fn calculate_next_index(current_index: usize, instruction: Instruction) -> usize {
    match instruction.operation {
        ACC => current_index + 1,
        JMP => ((current_index as i32) + instruction.argument) as usize,
        NOP => current_index + 1,
    }
}

#[cfg(test)]
mod tests {
    use adventofcode::read_lines_from_file;

    use super::*;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2020/day_8_1_example.txt";
        let input = read_lines_from_file(path);
        let expected: i32 = 5;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2020/day_8_1.txt";
        let input = read_lines_from_file(path);
        let expected: i32 = 1684;

        let actual = input.map(calculate_part_one);

        assert_eq!(expected, actual.unwrap());
    }

    #[test]
    fn part_two_with_example() {
        let path = "../../input/2020/day_8_2_example.txt";
        let input = read_lines_from_file(path);
        let expected: i32 = 8;

        let actual = input.map(calculate_part_two);

        assert_eq!(expected, actual.unwrap());
    }
}
