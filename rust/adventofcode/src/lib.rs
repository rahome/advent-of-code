use std::fmt::{Display, Formatter};
use std::fs;

/// Represents which source type to use for the puzzle.
pub enum Source {
    Example,
    Input,
}

impl Display for Source {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Source::Example => write!(f, "example"),
            Source::Input => write!(f, "input"),
        }
    }
}

/// Represents which part of the puzzle on a given day to perform.
pub enum Part {
    One,
    Two,
}

impl Display for Part {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Part::One => write!(f, "one"),
            Part::Two => write!(f, "two"),
        }
    }
}

/// Read the contents of a file on a given path.
pub fn read_contents_of_file(path: &str) -> Vec<String> {
    read_lines_from_file(path)
        .expect(&format!("Unable to read file at path {}", path).as_str())
}

/// Will attempt to read the contents of the file and split the contents based on the line separator.
pub fn read_lines_from_file(path: &str) -> Result<Vec<String>, std::io::Error> {
    fs::read_to_string(path)
        .map(read_lines_from_contents)
}

/// Will attempt to split the [content] into a vec based on the line separator.
fn read_lines_from_contents(content: String) -> Vec<String> {
    content.lines()
        .map(String::from)
        .collect()
}

/// Will iterate over a list of tuples and map the first tuple value, similar to [map_second].
pub fn map_first<T>(v: &Vec<(T, T)>) -> Vec<T> where T: Clone {
    v.iter().map(|(x, _)| x.clone()).collect()
}

/// Will iterate over a list of tuples and map the second tuple value, similar to [map_first].
pub fn map_second<T>(v: &Vec<(T, T)>) -> Vec<T> where T: Clone  {
    v.iter().map(|(_, x)| x.clone()).collect()
}

/// Will attempt to parse each value of a `Vec<String>` into a `Vec<T>` where `T` must conform to
/// the `std::str::FromStr` trait. Values that can not be parsed will be skipped.
pub fn map_parse<T: std::str::FromStr>(vec: &Vec<String>) -> Vec<T> {
    vec.iter()
        .filter_map(|v| {
            match v.parse::<T>() {
                Ok(x) => Some(x),
                Err(_) => {
                    eprintln!("Unable to parse {} to {}", v, std::any::type_name::<T>());
                    None
                }
            }
        })
        .collect()
}

/// Map a [Vec] of [String] to a [Vec] of [i32].
///
/// ```rust
/// # use adventofcode::map_to_i32;
/// let source = vec!["1".to_string(), "2".to_string(), "3".to_string(), "4".to_string()];
/// let values: Vec<i32> = map_to_i32(source);
///
/// assert_eq!(vec![1, 2, 3, 4], values);
/// ```
pub fn map_to_i32(vec: Vec<String>) -> Vec<i32> {
    map_parse(&vec)
}

/// Map a [Vec] of [String] to a [Vec] of [i64].
///
/// ```rust
/// # use adventofcode::map_to_i64;
/// let source = vec!["1".to_string(), "2".to_string(), "3".to_string(), "4".to_string()];
/// let values: Vec<i64> = map_to_i64(source);
///
/// assert_eq!(vec![1, 2, 3, 4], values);
/// ```
pub fn map_to_i64(vec: Vec<String>) -> Vec<i64> {
    map_parse(&vec)
}

/// Read character at index from String.
///
/// With a valid index, the character (as [String]) at the index will be returned as [Option.Some].
///
/// ```rust
/// # use adventofcode::character_at_index;
/// let value = String::from("abc");
/// let character = character_at_index(1, &value);
///
/// assert_eq!(character.is_some(), true);
/// assert_eq!(character.unwrap(), String::from("b"));
/// ```
///
/// With an invalid index, [Option.None] will be returned.
///
/// ```rust
/// # use adventofcode::character_at_index;
/// let value = String::from("abc");
/// let character = character_at_index(7, &value);
///
/// assert_eq!(character.is_none(), true);
/// ```
pub fn character_at_index(index: i32, s: &String) -> Option<String> {
    let index = index as usize;
    if index >= s.len() {
        return None;
    }

    return s.chars()
        .nth(index)
        .map(|v| { v.to_string() });
}
