use adventofcode::{map_first, map_parse, map_second};

fn calculate_part_one(input: Vec<String>) -> Option<isize> {
    let values = collect_first_and_last_value_for_each_line(input);
    let mut firsts = map_parse::<isize>(&map_first(&values));
    let mut seconds = map_parse::<isize>(&map_second(&values));
    firsts.sort();
    seconds.sort();

    Some(
        firsts
            .iter()
            .zip(seconds.iter())
            .map(|(a, b)| (a - b).abs())
            .sum::<isize>(),
    )
}

/// Will collect the first and last value, separated by whitespace, for each line. Only the first
/// and the last value will be collected, even if multiple whitespaces are present.
fn collect_first_and_last_value_for_each_line(lines: Vec<String>) -> Vec<(String, String)> {
    lines
        .into_iter()
        .map(|x| {
            let mut slices = x.split_whitespace();
            let first = slices.next().unwrap_or("");
            let last = slices.last().unwrap_or("");
            (String::from(first), String::from(last))
        })
        .collect()
}

fn calculate_part_two(input: Vec<String>) -> Option<usize> {
    let x = collect_first_and_last_value_for_each_line(input);
    let mut firsts = map_parse::<usize>(&map_first(&x));
    let mut seconds = map_parse::<usize>(&map_second(&x));
    firsts.sort();
    seconds.sort();

    Some(
        firsts
            .iter()
            .map(|left| {
                let x = seconds.iter().filter(|y| *y == left).count();
                left * x
            })
            .sum(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use adventofcode::read_lines_from_file;

    #[test]
    fn part_one_with_example() {
        let path = "../../input/2024/day_1_1_example.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(11);

        let value = input.map(calculate_part_one);

        assert_eq!(expected, value.unwrap());
    }

    #[test]
    fn part_one_with_input() {
        let path = "../../input/2024/day_1_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<isize> = Some(2742123);

        let value = input.map(calculate_part_one);

        assert_eq!(expected, value.unwrap());
    }

    #[test]
    fn part_two_with_example() {
        let path = "../../input/2024/day_1_1_example.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(31);

        let value = input.map(calculate_part_two);

        assert_eq!(expected, value.unwrap());
    }

    #[test]
    fn part_two_with_input() {
        let path = "../../input/2024/day_1_1.txt";
        let input = read_lines_from_file(path);
        let expected: Option<usize> = Some(21328497);

        let value = input.map(calculate_part_two);

        assert_eq!(expected, value.unwrap());
    }
}
